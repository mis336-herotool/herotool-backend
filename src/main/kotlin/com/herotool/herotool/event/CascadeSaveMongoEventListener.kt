package com.herotool.herotool.event

import com.herotool.herotool.annotation.CascadeSave
import com.herotool.herotool.utility.Logger
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.MongoOperations
import org.springframework.data.mongodb.core.mapping.DBRef
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener
import org.springframework.data.mongodb.core.mapping.event.BeforeConvertEvent
import org.springframework.stereotype.Component
import org.springframework.util.ReflectionUtils
import java.lang.reflect.Field

@Component
class CascadeSaveMongoEventListener(private val mongoOperations: MongoOperations) : AbstractMongoEventListener<Any>() {

    override fun onBeforeConvert(event: BeforeConvertEvent<Any>) {
        val source = event.source
        ReflectionUtils.doWithFields(source.javaClass, CascadeCallback(source, mongoOperations))
    }
}

class CascadeCallback(private val source: Any, private val mongoOperations: MongoOperations) : ReflectionUtils.FieldCallback {

    private val logger by Logger()

    override fun doWith(field: Field) {
        ReflectionUtils.makeAccessible(field)

        if (field.isAnnotationPresent(DBRef::class.java) && field.isAnnotationPresent(CascadeSave::class.java)) {
            val fieldValue = field.get(source)

            if (fieldValue != null) {
                when(fieldValue) {
                    is List<*> -> {
                        fieldValue.forEach { doWithField(it) }
                    }
                    else -> {
                        logger.debug("Field type: ${fieldValue.javaClass.simpleName}")
                        doWithField(fieldValue)
                    }
                }
            }
        }
    }

    private fun doWithField(fieldValue: Any?) {
        val callback = FieldCallback()
        ReflectionUtils.doWithFields(fieldValue!!.javaClass, callback)
        mongoOperations.save(fieldValue)
    }
}

class FieldCallback : ReflectionUtils.FieldCallback {
    private var idFound = false

    override fun doWith(field: Field) {
        ReflectionUtils.makeAccessible(field)

        if (field.isAnnotationPresent(Id::class.java)) {
            idFound = true
        }
    }

    fun isIdFound() = idFound
}