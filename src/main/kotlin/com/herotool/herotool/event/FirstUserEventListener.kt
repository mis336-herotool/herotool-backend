package com.herotool.herotool.event

import com.herotool.herotool.domain.dto.request.EmployeeCreateRequestDto
import com.herotool.herotool.domain.enums.MaritalStatus
import com.herotool.herotool.domain.enums.Title
import com.herotool.herotool.domain.enums.UserRole
import com.herotool.herotool.service.EmployeeService
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component
import java.time.LocalDate

@Component
class FirstUserEventListener(private val employeeService: EmployeeService) {

    @EventListener(ApplicationReadyEvent::class)
    fun run() {
        if (employeeService.count() == 0L) {
            val adminUserCreateRequest = EmployeeCreateRequestDto(
                name = "admin",
                surname = "admin",
                birthDate = LocalDate.now(),
                roles = listOf(UserRole.ADMIN, UserRole.HR),
                title = Title.SYSTEM_ADMIN,
                position = "ADMIN",
                department = "HR",
                phoneNumber = "",
                email = "admin@herotool.com",
                address = "",
                jobStartDate = LocalDate.now(),
                maritalStatus = MaritalStatus.SINGLE,
                jobEndDate = null,
                manager = null
            )
            employeeService.create(adminUserCreateRequest)
        }
    }
}