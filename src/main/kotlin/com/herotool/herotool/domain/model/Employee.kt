package com.herotool.herotool.domain.model

import com.github.pozo.KotlinBuilder
import com.herotool.herotool.annotation.CascadeSave
import com.herotool.herotool.domain.enums.MaritalStatus
import com.herotool.herotool.domain.enums.Title
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.DBRef
import org.springframework.data.mongodb.core.mapping.Document
import java.time.LocalDate

@Document
@KotlinBuilder
data class Employee(
    val name: String,
    val surname: String,
    val email: String,
    val birthDate: LocalDate,
    val title: Title,
    @DBRef @CascadeSave val position: Position,
    val department: Department,
    val phoneNumber: String,
    val address: String,
    val jobStartDate: LocalDate,
    @DBRef @CascadeSave val account: User,
    @DBRef @CascadeSave val benefits: List<Benefit> = emptyList(),
    @DBRef @CascadeSave val salaries: List<Salary> = emptyList(),
    @DBRef val manager: Employee? = null,
    val jobEndDate: LocalDate? = null,
    val maritalStatus: MaritalStatus = MaritalStatus.SINGLE,

    @Id var id: String? = null
)