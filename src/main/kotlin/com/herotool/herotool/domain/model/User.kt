package com.herotool.herotool.domain.model

import com.github.pozo.KotlinBuilder
import com.herotool.herotool.domain.enums.UserRole
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails

@Document
@KotlinBuilder
data class User(
    val email: String,
    private val password: String,
    val roles: List<UserRole>,
    private val enabled: Boolean = true,
    @Id var id: String? = null
) : UserDetails {

    override fun getAuthorities(): List<GrantedAuthority> {
        return roles.toAuthority()
    }

    override fun isEnabled(): Boolean {
        return enabled
    }

    override fun getUsername(): String {
        return email
    }

    override fun isCredentialsNonExpired(): Boolean {
        return true
    }

    override fun getPassword(): String {
        return password
    }

    override fun isAccountNonExpired(): Boolean {
        return true
    }

    override fun isAccountNonLocked(): Boolean {
        return true
    }

    private fun List<UserRole>.toAuthority(): List<GrantedAuthority> {
        return this.map { GrantedAuthority { "ROLE_${it.name}" } }
    }
}