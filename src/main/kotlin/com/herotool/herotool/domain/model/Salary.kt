package com.herotool.herotool.domain.model

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.math.BigDecimal
import java.time.LocalDate

@Document
data class Salary(
    val value: BigDecimal,
    val startDate: LocalDate,
    val endDate: LocalDate? = null,
    @Id var id: String? = null
) {

    fun isActive(): Boolean {
        return startDate <= LocalDate.now() && (endDate == null || endDate > LocalDate.now())
    }
}