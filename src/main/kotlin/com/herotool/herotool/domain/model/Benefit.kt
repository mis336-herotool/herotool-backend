package com.herotool.herotool.domain.model

import com.github.pozo.KotlinBuilder
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.math.BigDecimal

@Document
@KotlinBuilder
data class Benefit(
    val name: String,
    val value: BigDecimal,
    @Id var id: String? = null
)