package com.herotool.herotool.domain.model

import com.github.pozo.KotlinBuilder
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document
@KotlinBuilder
data class Department(
    val name: String,
    @Id var id: String? = null
)