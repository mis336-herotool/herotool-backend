package com.herotool.herotool.domain.model

import com.github.pozo.KotlinBuilder
import com.herotool.herotool.annotation.CascadeSave
import com.herotool.herotool.domain.enums.AttendanceStatus
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.DBRef
import org.springframework.data.mongodb.core.mapping.Document
import java.time.LocalDate

@Document
@KotlinBuilder
data class Attendance(
    @DBRef @CascadeSave val employee: Employee,
    val startDate: LocalDate,
    val endDate: LocalDate,
    val reason: String?,
    val status: AttendanceStatus = AttendanceStatus.PENDING,

    @Id var id: String? = null
)

