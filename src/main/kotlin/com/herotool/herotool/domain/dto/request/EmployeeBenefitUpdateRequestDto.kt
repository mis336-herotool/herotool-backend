package com.herotool.herotool.domain.dto.request

import java.math.BigDecimal

data class EmployeeBenefitUpdateRequestDto(
    val benefits: List<String>,
    val salary: BigDecimal
)