package com.herotool.herotool.domain.dto.response

import com.github.pozo.KotlinBuilder

@KotlinBuilder
data class EmployeeProfileResponseDto(
    val id: String,
    val name: String,
    val surname: String,
    val email: String,
    val phoneNumber: String,
    val address: String,
    val title: String,
    val department: String,
    val manager: String?
)