package com.herotool.herotool.domain.dto.request

data class ChangePasswordRequestDto(
    val previousPassword: String,
    val newPassword: String
)