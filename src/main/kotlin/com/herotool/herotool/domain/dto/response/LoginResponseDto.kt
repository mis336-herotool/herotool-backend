package com.herotool.herotool.domain.dto.response

data class LoginResponseDto(val accessToken: String)