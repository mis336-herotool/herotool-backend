package com.herotool.herotool.domain.dto.response

import com.github.pozo.KotlinBuilder
import com.herotool.herotool.domain.enums.MaritalStatus
import com.herotool.herotool.domain.enums.Title
import com.herotool.herotool.domain.enums.UserRole
import org.springframework.format.annotation.DateTimeFormat
import org.springframework.hateoas.EntityModel
import java.time.LocalDate

@KotlinBuilder
data class EmployeeResponseDto(
    val id: String,
    val name: String,
    val surname: String,
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    val birthDate: LocalDate,
    val roles: List<UserRole>,
    val title: Title,
    val position: String,
    val department: String,
    val phoneNumber: String,
    val email: String,
    val address: String,
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    val jobStartDate: LocalDate,
    val manager: String?,
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    val jobEndDate: LocalDate?,
    val maritalStatus: MaritalStatus
)