package com.herotool.herotool.domain.dto.request

import java.math.BigDecimal

data class BenefitCreateRequestDto(val name: String, val value: BigDecimal)