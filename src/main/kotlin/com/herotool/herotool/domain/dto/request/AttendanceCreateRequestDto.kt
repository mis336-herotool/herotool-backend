package com.herotool.herotool.domain.dto.request

import java.time.LocalDate

data class AttendanceCreateRequestDto(
    val startDate: LocalDate,
    val endDate: LocalDate,
    val reason: String?
)

