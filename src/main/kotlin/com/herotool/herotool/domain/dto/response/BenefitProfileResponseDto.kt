package com.herotool.herotool.domain.dto.response

import com.github.pozo.KotlinBuilder
import org.springframework.format.annotation.DateTimeFormat
import java.math.BigDecimal
import java.time.LocalDate

data class BenefitProfileResponseDto(
    val activeSalary: String?,
    val salaryHistory: List<SalaryResponseDto>,
    val benefits: List<BenefitResponseDto>,
    val departmentAverageSalary: BigDecimal
)

@KotlinBuilder
data class SalaryResponseDto(
    val salary: BigDecimal,
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    val startDate: LocalDate,
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    val endDate: LocalDate?
)