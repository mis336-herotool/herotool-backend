package com.herotool.herotool.domain.dto.response

data class ErrorResponseDto(val status: String, val message: String, val text: String)