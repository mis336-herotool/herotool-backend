package com.herotool.herotool.domain.dto.request

import java.math.BigDecimal

data class BenefitUpdateRequestDto(
    val name: String, val value: BigDecimal
)