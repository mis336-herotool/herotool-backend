package com.herotool.herotool.domain.dto.request

import com.herotool.herotool.domain.enums.AttendanceStatus
import java.time.LocalDate

data class AttendanceUpdateRequestDto(
    val startDate: LocalDate,
    val endDate: LocalDate,
    val reason: String?,
    val status: AttendanceStatus
)