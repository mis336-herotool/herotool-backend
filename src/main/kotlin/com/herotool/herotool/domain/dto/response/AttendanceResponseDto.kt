package com.herotool.herotool.domain.dto.response

import com.github.pozo.KotlinBuilder
import com.herotool.herotool.domain.enums.AttendanceStatus
import java.time.LocalDate

@KotlinBuilder
data class AttendanceResponseDto(
    val id: String,
    val employee: EmployeeResponseDto,
    val startDate: LocalDate,
    val endDate: LocalDate,
    val reason: String?,
    val status: AttendanceStatus
)