package com.herotool.herotool.domain.dto.request

data class LoginRequestDto(val email: String, val password: String)