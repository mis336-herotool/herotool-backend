package com.herotool.herotool.domain.dto.response

import com.herotool.herotool.domain.enums.Title
import java.math.BigDecimal

data class EmployeeBenefitResponseDto(
    val id: String,
    val name: String,
    val surname: String,
    val title: Title,
    val position: String,
    val department: String,
    val benefits: List<String>,
    val salary: BigDecimal?
)