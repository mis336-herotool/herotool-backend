package com.herotool.herotool.domain.dto.response

import com.github.pozo.KotlinBuilder
import java.math.BigDecimal

@KotlinBuilder
data class BenefitResponseDto(val id: String, val name: String, val value: BigDecimal)