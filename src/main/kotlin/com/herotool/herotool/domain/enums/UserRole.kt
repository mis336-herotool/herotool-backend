package com.herotool.herotool.domain.enums

enum class UserRole {
    ADMIN, HR, MANAGEMENT, EMPLOYEE
}