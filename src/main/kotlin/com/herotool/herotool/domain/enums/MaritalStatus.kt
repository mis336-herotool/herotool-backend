package com.herotool.herotool.domain.enums

enum class MaritalStatus {
    MARRIED,
    SINGLE,
    WIDOWED,
    DIVORCED
}