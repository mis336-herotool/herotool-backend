package com.herotool.herotool.domain.enums

enum class AttendanceStatus {
    ACCEPTED, REJECTED, PENDING
}