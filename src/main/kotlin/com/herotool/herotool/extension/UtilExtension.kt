package com.herotool.herotool.extension

import org.springframework.core.convert.ConversionService
import org.springframework.data.domain.Page
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.data.web.PagedResourcesAssembler
import org.springframework.hateoas.EntityModel
import org.springframework.hateoas.PagedModel
import org.springframework.http.ResponseEntity
import java.time.LocalDateTime
import java.time.ZoneId
import java.util.Date

/**
 * @receiver[MongoRepository]
 * @param[T] Entity type
 * @param[ID] Entity id type
 * @return[T]
 */
fun <T, ID> MongoRepository<T, ID>.findByIdOrNull(id: ID?): T? {
    return if (id != null) this.findById(id).orElse(null) else null
}

/**
 * @receiver[ConversionService]
 * @param[T] conversion target type
 * @param[block] source block
 * @return conversion result
 */
inline fun <reified T> ConversionService.convert(block: () -> Any): T {
    val entity = block.invoke()
    return this.convert(entity, T::class.java)!!
}

/**
 * @receiver[ConversionService]
 * @param[T] conversion target type
 * @param[block] source block
 * @return wraps conversion result with ResponseEntity
 */
inline fun <reified T> ConversionService.convertResponseBody(block: () -> Any): ResponseEntity<T> {
    val response = this.convert<T>(block)
    return ResponseEntity.ok(response)
}

/**
 * @receiver[PagedResourcesAssembler]
 * @param[T] PagedResource type
 * @param[page] page returned from paged repository
 * @return[PagedModel]
 */
@Suppress("UNCHECKED_CAST")
inline fun <reified T> PagedResourcesAssembler<T>.toPagedModel(page: Page<T>): PagedModel<EntityModel<T>> {
    return if (page.isEmpty) {
        this.toEmptyModel(page, T::class.java)
    } else {
        this.toModel(page)
    } as PagedModel<EntityModel<T>>
}

/**
 * @receiver[LocalDateTime]
 * @return[Date]
 */
fun LocalDateTime.toDate(): Date {
    return Date.from(this.atZone(ZoneId.systemDefault()).toInstant())
}

