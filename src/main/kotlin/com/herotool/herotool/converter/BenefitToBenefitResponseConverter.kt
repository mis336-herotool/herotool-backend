package com.herotool.herotool.converter

import com.herotool.herotool.converter.mapper.BenefitToBenefitResponseMapper
import com.herotool.herotool.domain.dto.response.BenefitResponseDto
import com.herotool.herotool.domain.model.Benefit
import org.springframework.core.convert.converter.Converter
import org.springframework.stereotype.Component

@Component
class BenefitToBenefitResponseConverter(private val benefitToBenefitResponseMapper: BenefitToBenefitResponseMapper): Converter<Benefit, BenefitResponseDto> {

    override fun convert(source: Benefit): BenefitResponseDto {
        return benefitToBenefitResponseMapper.map(source)
    }
}