package com.herotool.herotool.converter

import com.herotool.herotool.converter.mapper.EmployeeToEmployeeResponseMapper
import com.herotool.herotool.domain.dto.response.EmployeeResponseDto
import com.herotool.herotool.domain.model.Employee
import org.springframework.core.convert.converter.Converter
import org.springframework.stereotype.Component

@Component
class EmployeeToEmployeeResponseConverter(private val employeeToEmployeeResponseMapper: EmployeeToEmployeeResponseMapper):
    Converter<Employee, EmployeeResponseDto> {

    override fun convert(employee: Employee): EmployeeResponseDto {
        return employeeToEmployeeResponseMapper.map(employee)
    }
}