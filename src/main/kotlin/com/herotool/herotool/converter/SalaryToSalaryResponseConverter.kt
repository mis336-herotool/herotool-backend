package com.herotool.herotool.converter

import com.herotool.herotool.converter.mapper.SalaryToSalaryResponseMapper
import com.herotool.herotool.domain.dto.response.SalaryResponseDto
import com.herotool.herotool.domain.model.Salary
import org.springframework.core.convert.converter.Converter
import org.springframework.stereotype.Component

@Component
class SalaryToSalaryResponseConverter(private val salaryMapper: SalaryToSalaryResponseMapper) : Converter<Salary, SalaryResponseDto> {

    override fun convert(source: Salary): SalaryResponseDto {
        return salaryMapper.map(source)
    }
}