package com.herotool.herotool.converter

import com.herotool.herotool.converter.mapper.EmployeeToProfileResponseMapper
import com.herotool.herotool.domain.dto.response.EmployeeProfileResponseDto
import com.herotool.herotool.domain.model.Employee
import org.springframework.core.convert.converter.Converter
import org.springframework.stereotype.Component

@Component
class EmployeeToProfileResponseConverter(private val profileMapper: EmployeeToProfileResponseMapper) : Converter<Employee, EmployeeProfileResponseDto> {

    override fun convert(source: Employee): EmployeeProfileResponseDto? {
        return profileMapper.map(source)
    }
}