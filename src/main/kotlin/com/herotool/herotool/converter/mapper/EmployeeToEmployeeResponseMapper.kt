package com.herotool.herotool.converter.mapper

import com.herotool.herotool.domain.dto.response.EmployeeResponseDto
import com.herotool.herotool.domain.model.Employee
import org.mapstruct.Mapper

@Mapper
abstract class EmployeeToEmployeeResponseMapper {

    fun map(employee: Employee): EmployeeResponseDto {
        return with(employee) {
            EmployeeResponseDto(
                id!!,
                name,
                surname,
                birthDate,
                account.roles,
                title,
                position.name,
                department.name,
                phoneNumber,
                email,
                address,
                jobStartDate,
                manager?.id,
                jobEndDate,
                maritalStatus
            )
        }
    }
}