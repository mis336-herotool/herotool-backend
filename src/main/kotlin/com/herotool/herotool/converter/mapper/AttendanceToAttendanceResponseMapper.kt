package com.herotool.herotool.converter.mapper

import com.herotool.herotool.domain.dto.response.AttendanceResponseDto
import com.herotool.herotool.domain.dto.response.EmployeeResponseDto
import com.herotool.herotool.domain.model.Attendance
import com.herotool.herotool.domain.model.Employee
import org.mapstruct.Mapper

@Mapper
abstract class AttendanceToAttendanceResponseMapper {

    fun map(attendance: Attendance, employeeConverter: (Employee) -> (EmployeeResponseDto)): AttendanceResponseDto {
        return with(attendance) { AttendanceResponseDto(id!!, employeeConverter.invoke(employee), startDate, endDate, reason, status) }
    }
}