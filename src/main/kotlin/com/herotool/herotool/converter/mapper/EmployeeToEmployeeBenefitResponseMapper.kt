package com.herotool.herotool.converter.mapper

import com.herotool.herotool.domain.dto.response.BenefitResponseDto
import com.herotool.herotool.domain.dto.response.EmployeeBenefitResponseDto
import com.herotool.herotool.domain.model.Benefit
import com.herotool.herotool.domain.model.Employee
import org.mapstruct.Mapper

@Mapper
abstract class EmployeeToEmployeeBenefitResponseMapper {

    fun map(employee: Employee): EmployeeBenefitResponseDto {
        return with(employee) {
            EmployeeBenefitResponseDto(
                id!!,
                name,
                surname,
                title,
                position.name,
                department.name,
                benefits.map { it.id!! },
                salaries.find { it.isActive() }?.value
            )
        }

    }
}