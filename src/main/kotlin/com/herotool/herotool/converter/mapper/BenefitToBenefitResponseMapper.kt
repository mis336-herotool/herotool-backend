package com.herotool.herotool.converter.mapper

import com.herotool.herotool.domain.dto.response.BenefitResponseDto
import com.herotool.herotool.domain.model.Benefit
import org.mapstruct.Mapper

@Mapper
interface BenefitToBenefitResponseMapper {

    fun map(benefit: Benefit): BenefitResponseDto
}