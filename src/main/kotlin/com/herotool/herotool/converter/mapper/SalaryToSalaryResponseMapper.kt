package com.herotool.herotool.converter.mapper

import com.herotool.herotool.domain.dto.response.SalaryResponseDto
import com.herotool.herotool.domain.model.Salary
import org.mapstruct.Mapper
import org.mapstruct.Mapping

@Mapper
interface SalaryToSalaryResponseMapper {

    @Mapping(source = "value", target = "salary")
    fun map(source: Salary): SalaryResponseDto
}