package com.herotool.herotool.converter.mapper

import com.herotool.herotool.domain.dto.response.EmployeeProfileResponseDto
import com.herotool.herotool.domain.model.Employee
import org.mapstruct.Mapper

@Mapper
abstract class EmployeeToProfileResponseMapper {

    fun map(source: Employee): EmployeeProfileResponseDto {
        return with(source) {
            EmployeeProfileResponseDto(
                id = id!!,
                name = name,
                surname = surname,
                email = email,
                phoneNumber = phoneNumber,
                address = address,
                title = title.name,
                department = department.name,
                manager = if (manager != null) "${manager.name} ${manager.surname}" else manager
            )
        }
    }
}