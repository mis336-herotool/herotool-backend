package com.herotool.herotool.converter

import com.herotool.herotool.converter.mapper.AttendanceToAttendanceResponseMapper
import com.herotool.herotool.domain.dto.response.AttendanceResponseDto
import com.herotool.herotool.domain.model.Attendance
import org.springframework.core.convert.converter.Converter
import org.springframework.stereotype.Component

@Component
class AttendanceToAttendanceResponseConverter(
    private val attendanceToAttendanceResponseMapper: AttendanceToAttendanceResponseMapper,
    private val employeeResponseConverter: EmployeeToEmployeeResponseConverter
) : Converter<Attendance, AttendanceResponseDto> {

    override fun convert(attendance: Attendance): AttendanceResponseDto {
        return attendanceToAttendanceResponseMapper.map(attendance, employeeResponseConverter::convert)
    }
}