package com.herotool.herotool.converter

import com.herotool.herotool.converter.mapper.EmployeeToEmployeeBenefitResponseMapper
import com.herotool.herotool.domain.dto.response.EmployeeBenefitResponseDto
import com.herotool.herotool.domain.model.Employee
import org.springframework.core.convert.converter.Converter
import org.springframework.stereotype.Component

@Component
class EmployeeToEmployeeBenefitResponseConverter(
    private val employeeToEmployeeBenefitResponseMapper: EmployeeToEmployeeBenefitResponseMapper
) : Converter<Employee, EmployeeBenefitResponseDto> {

    override fun convert(source: Employee): EmployeeBenefitResponseDto {
        return employeeToEmployeeBenefitResponseMapper.map(source)
    }
}