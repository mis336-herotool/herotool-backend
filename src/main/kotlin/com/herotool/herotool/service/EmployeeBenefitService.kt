package com.herotool.herotool.service

import com.herotool.herotool.domain.dto.request.EmployeeBenefitUpdateRequestDto
import com.herotool.herotool.domain.dto.response.EmployeeBenefitResponseDto
import com.herotool.herotool.domain.model.Salary
import com.herotool.herotool.extension.convert
import com.herotool.herotool.repository.SalaryRepository
import com.querydsl.core.types.Predicate
import org.springframework.core.convert.ConversionService
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import java.time.LocalDate

@Service
class EmployeeBenefitService(
    private val employeeService: EmployeeService,
    private val benefitService: BenefitService,
    private val conversionService: ConversionService,
    private val salaryRepository: SalaryRepository
) {

    fun getById(id: String): EmployeeBenefitResponseDto {
        return conversionService.convert { employeeService.getById(id) }
    }

    fun getAll(predicate: Predicate?, pageable: Pageable): Page<EmployeeBenefitResponseDto> {
        return employeeService.getAll(predicate, pageable).map {
            conversionService.convert<EmployeeBenefitResponseDto> { it }
        }
    }

    fun update(id: String, updateRequest: EmployeeBenefitUpdateRequestDto): EmployeeBenefitResponseDto {
        val employee = employeeService.getById(id)
        val benefits = updateRequest.benefits.map(benefitService::getById)
        val salaries = employee.salaries.map { if (it.isActive()) it.copy(endDate = LocalDate.now()) else it } as MutableList<Salary>
        salaries.add(salaryRepository.save(Salary(updateRequest.salary, LocalDate.now())))
        return conversionService.convert { employeeService.update(employee.copy(benefits = benefits, salaries = salaries)) }
    }
}
