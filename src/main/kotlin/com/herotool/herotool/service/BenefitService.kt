package com.herotool.herotool.service

import com.herotool.herotool.domain.dto.request.BenefitCreateRequestDto
import com.herotool.herotool.domain.dto.request.BenefitUpdateRequestDto
import com.herotool.herotool.domain.dto.response.BenefitProfileResponseDto
import com.herotool.herotool.domain.dto.response.BenefitResponseDto
import com.herotool.herotool.domain.dto.response.SalaryResponseDto
import com.herotool.herotool.domain.model.Benefit
import com.herotool.herotool.domain.model.User
import com.herotool.herotool.exception.ResourceNotFoundException
import com.herotool.herotool.extension.convert
import com.herotool.herotool.extension.findByIdOrNull
import com.herotool.herotool.repository.BenefitRepository
import com.herotool.herotool.utility.average
import com.querydsl.core.types.Predicate
import org.springframework.core.convert.ConversionService
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import java.math.RoundingMode

@Service
class BenefitService(
    private val benefitRepository: BenefitRepository,
    private val employeeService: EmployeeService,
    private val conversionService: ConversionService
) {

    fun create(benefit: BenefitCreateRequestDto): Benefit {
        return with(benefit) { benefitRepository.save(Benefit(name, value)) }
    }

    fun update(id: String, benefitRequest: BenefitUpdateRequestDto): Benefit {
        val benefit = getById(id)
        return with(benefitRequest) { benefitRepository.save(benefit.copy(name = name, value = value)) }
    }

    fun findById(id: String): Benefit? {
        return benefitRepository.findByIdOrNull(id)
    }

    fun getById(id: String): Benefit {
        return findById(id) ?: throw ResourceNotFoundException()
    }

    fun getAll(predicate: Predicate?, pageable: Pageable): Page<Benefit> {
        return if (predicate != null) {
            benefitRepository.findAll(predicate, pageable)
        } else {
            benefitRepository.findAll(pageable)
        }
    }

    fun getAllWithoutPagination(): List<Benefit> {
        return benefitRepository.findAll()
    }

    fun getBenefitProfile(user: User): BenefitProfileResponseDto {
        val employee = employeeService.getByAccountId(user.id!!)
        val activeSalary = employee.salaries.find { it.isActive() }

        val departmentAverage = employeeService.getAllByDepartment(employee.department.id!!)
            .mapNotNull { emp -> emp.salaries.find { it.isActive() }?.value }
            .average()

        val salaryHistory = employee.salaries
            .map { conversionService.convert<SalaryResponseDto> { it } }

        val benefits = employee.benefits
            .map { conversionService.convert<BenefitResponseDto> { it } }

        return BenefitProfileResponseDto(
            activeSalary = activeSalary?.value?.setScale(2, RoundingMode.HALF_UP)?.toPlainString(),
            departmentAverageSalary = departmentAverage.setScale(2, RoundingMode.HALF_UP),
            salaryHistory = salaryHistory,
            benefits = benefits
        )
    }
}