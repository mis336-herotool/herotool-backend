package com.herotool.herotool.service

import com.herotool.herotool.configuration.properties.AuthenticationProperties
import com.herotool.herotool.extension.toDate
import com.herotool.herotool.utility.Logger
import io.jsonwebtoken.Claims
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.stereotype.Service
import java.security.KeyFactory
import java.security.spec.PKCS8EncodedKeySpec
import java.security.spec.X509EncodedKeySpec
import java.time.LocalDateTime
import java.util.Base64

@Service
class TokenService(private val authenticationProperties: AuthenticationProperties) {

    private val logger by Logger()

    fun getClaims(token: String): Claims {
        val publicKeyByteArray = Base64.getDecoder().decode(authenticationProperties.publicKey)
        val publicKeySpec = X509EncodedKeySpec(publicKeyByteArray)
        val keyFactory = KeyFactory.getInstance("RSA")
        val publicKey = keyFactory.generatePublic(publicKeySpec)

        return try {
            Jwts.parser()
                .setSigningKey(publicKey)
                .requireIssuer(TOKEN_ISSUER)
                .parseClaimsJws(token)
                .body ?: throw IllegalStateException(EMPTY_CLAIMS_ERROR)
        } catch (ex: Exception) {
            throw BadCredentialsException(INVALID_TOKEN)
        }
    }

    fun generateToken(userId: String, userRoles: List<Any>): String {
        logger.info("Generating token for user: $userId, with authorities: $userRoles")
        val issuedAt = LocalDateTime.now()
        val expiresAt = issuedAt.plusSeconds(TOKEN_DURATION)
        val claims = mapOf(
            "id" to userId,
            "roles" to userRoles
        )

        val privateKeyByteArray = Base64.getDecoder().decode(authenticationProperties.privateKey)
        val privateKeySpec = PKCS8EncodedKeySpec(privateKeyByteArray)
        val keyFactory = KeyFactory.getInstance("RSA")
        val privateKey = keyFactory.generatePrivate(privateKeySpec)

        return Jwts.builder()
            .setClaims(claims)
            .setIssuedAt(issuedAt.toDate())
            .setExpiration(expiresAt.toDate())
            .setIssuer(TOKEN_ISSUER)
            .signWith(SignatureAlgorithm.RS256, privateKey)
            .compact()
    }

    companion object {
        private const val EMPTY_CLAIMS_ERROR = "error.auth.empty-claims"
        private const val INVALID_TOKEN = "error.auth.invalid-token"
        private const val TOKEN_DURATION = 432_000L
        private const val TOKEN_ISSUER = "HeroTool"
    }
}