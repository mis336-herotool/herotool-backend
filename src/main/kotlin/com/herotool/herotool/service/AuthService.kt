package com.herotool.herotool.service

import com.herotool.herotool.domain.model.User
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.stereotype.Service

@Service
class AuthService(private val authenticationManager: AuthenticationManager, private val userService: UserService, private val tokenService: TokenService) {

    fun login(email: String, password: String): String {
        authenticationManager.authenticate(UsernamePasswordAuthenticationToken(email, password))
        val user = userService.loadUserByUsername(email) as User
        return tokenService.generateToken(user.id!!, user.authorities.map { it.authority })
    }

    fun changePassword(email: String, newPassword: String) {
        // TODO:
    }
}
