package com.herotool.herotool.service

import com.herotool.herotool.domain.dto.request.AttendanceCreateRequestDto
import com.herotool.herotool.domain.dto.request.AttendanceUpdateRequestDto
import com.herotool.herotool.domain.enums.UserRole
import com.herotool.herotool.domain.model.Attendance
import com.herotool.herotool.domain.model.Employee
import com.herotool.herotool.domain.model.QAttendance
import com.herotool.herotool.domain.model.QEmployee
import com.herotool.herotool.exception.ResourceNotFoundException
import com.herotool.herotool.exception.UnauthorizedAccessException
import com.herotool.herotool.extension.findByIdOrNull
import com.herotool.herotool.repository.AttendanceRepository
import com.herotool.herotool.repository.EmployeeRepository
import com.herotool.herotool.utility.checkAuthority
import com.herotool.herotool.utility.getPrincipal
import com.querydsl.core.types.Predicate
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service

@Service
class AttendanceService(private val attendanceRepository: AttendanceRepository, private val employeeRepository: EmployeeRepository) {

    fun create(attendanceRequest: AttendanceCreateRequestDto, employee: Employee): Attendance {
        val attendance = with(attendanceRequest) { Attendance(employee, startDate, endDate, reason) }
        return attendanceRepository.save(attendance)
    }

    fun update(id: String, updateRequest: AttendanceUpdateRequestDto): Attendance {
        val record = attendanceRepository.findByIdOrNull(id) ?: throw ResourceNotFoundException()
        val updatedRecord = with(updateRequest) { record.copy(startDate = startDate, endDate = endDate, reason = reason, status = status) }
        return attendanceRepository.save(updatedRecord)
    }

    fun getAll(pageable: Pageable, predicate: Predicate?): Page<Attendance> {
        val accountId = getPrincipal()?.id
        val employee = accountId?.let { employeeRepository.findByAccountId(it) }
        return when {
            checkAuthority(UserRole.ADMIN) || checkAuthority(UserRole.HR) -> {
                if (predicate != null) {
                    attendanceRepository.findAll(predicate, pageable)
                } else {
                    attendanceRepository.findAll(pageable)
                }
            }
            checkAuthority(UserRole.MANAGEMENT) -> {
                val subordinateIds = employeeRepository.findAllByManagerId(employee?.id!!).map { it.id!! }
                QAttendance.attendance.employee.id.`in`(subordinateIds)
                if (predicate != null) {
                    attendanceRepository.findAll(QEmployee.employee.id.`in`(subordinateIds), pageable)
                } else {
                    attendanceRepository.findAllByEmployeeIdIn(subordinateIds, pageable)
                }
            }
            checkAuthority(UserRole.EMPLOYEE) -> {
                if (predicate != null) {
                    val query = QAttendance.attendance.employee
                    attendanceRepository.findAll(query.id.eq(employee?.id!!).and(predicate), pageable)
                } else {
                    attendanceRepository.findAllByEmployeeId(employee?.id!!, pageable)
                }
            }
            else -> throw UnauthorizedAccessException()
        }
    }

    fun getById(id: String): Attendance {
        return attendanceRepository.findByIdOrNull(id) ?: throw ResourceNotFoundException()
    }

    fun getPendingRequests(accountId: String): List<Attendance> {
        val manager = employeeRepository.findByIdOrNull(accountId) ?: throw ResourceNotFoundException("Manager not found")
        val employees = employeeRepository.findAllByManagerId(manager.id!!)
        return employees.map { attendanceRepository.findAllByEmployeeId(it.id!!) }
            .flatten()
    }
}