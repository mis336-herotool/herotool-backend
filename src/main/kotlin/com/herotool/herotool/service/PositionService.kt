package com.herotool.herotool.service

import com.herotool.herotool.domain.model.Position
import com.herotool.herotool.exception.ResourceNotFoundException
import com.herotool.herotool.extension.findByIdOrNull
import com.herotool.herotool.repository.PositionRepository
import org.springframework.stereotype.Service

@Service
class PositionService(private val positionRepository: PositionRepository) {

    fun create(position: Position) = positionRepository.save(position)

    fun findById(positionId: String): Position? {
        return positionRepository.findByIdOrNull(positionId)
    }

    fun findByName(name: String): Position? {
        return positionRepository.findByName(name)
    }

    fun findByNameOrCreate(name: String): Position {
        return findByName(name) ?: create(Position(name))
    }

    fun getById(positionId: String): Position {
        return findById(positionId) ?: throw ResourceNotFoundException(POSITION_NOT_FOUND)
    }

    fun getByName(name: String): Position {
        return findByName(name) ?: throw ResourceNotFoundException(POSITION_NOT_FOUND)
    }

    companion object {
        private const val POSITION_NOT_FOUND = "error.position.not-found"
    }
}