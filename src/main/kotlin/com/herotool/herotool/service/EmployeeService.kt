package com.herotool.herotool.service

import com.herotool.herotool.domain.dto.request.AttendanceCreateRequestDto
import com.herotool.herotool.domain.dto.request.ChangePasswordRequestDto
import com.herotool.herotool.domain.dto.request.EmployeeCreateRequestDto
import com.herotool.herotool.domain.dto.request.EmployeeUpdateRequestDto
import com.herotool.herotool.domain.dto.response.EmployeeProfileResponseDto
import com.herotool.herotool.domain.model.Attendance
import com.herotool.herotool.domain.model.Employee
import com.herotool.herotool.domain.model.User
import com.herotool.herotool.exception.ResourceNotFoundException
import com.herotool.herotool.extension.convert
import com.herotool.herotool.extension.findByIdOrNull
import com.herotool.herotool.repository.EmployeeRepository
import com.querydsl.core.types.Predicate
import org.springframework.core.convert.ConversionService
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service

@Service
class EmployeeService(
    private val employeeRepository: EmployeeRepository,
    private val positionService: PositionService,
    private val departmentService: DepartmentService,
    private val userService: UserService,
    private val attendanceService: AttendanceService,
    private val conversionService: ConversionService
) {

    fun findById(employeeId: String): Employee? {
        return employeeRepository.findByIdOrNull(employeeId)
    }

    fun getById(employeeId: String): Employee {
        return findById(employeeId) ?: throw ResourceNotFoundException(EMPLOYEE_NOT_FOUND)
    }

    fun getAll(predicate: Predicate?, pageable: Pageable): Page<Employee> {
        return if (predicate != null) {
            employeeRepository.findAll(predicate, pageable)
        } else {
            employeeRepository.findAll(pageable)
        }
    }

    fun findAll(): List<Employee> {
        return employeeRepository.findAll()
    }

    fun count(): Long {
        return employeeRepository.count()
    }

    fun create(employeeRequest: EmployeeCreateRequestDto): Employee {
        val user = userService.findByEmail(employeeRequest.email) ?: userService.create(email = employeeRequest.email, roles = employeeRequest.roles)
        val position = positionService.findByNameOrCreate(employeeRequest.position)
        val department = departmentService.findByNameOrCreate(employeeRequest.department)
        val manager = employeeRepository.findByIdOrNull(employeeRequest.manager)
        val employee = with(employeeRequest) {
            Employee(
                name = name,
                surname = surname,
                email = email,
                birthDate = birthDate,
                title = title,
                position = position,
                department = department,
                phoneNumber = phoneNumber,
                address = address,
                jobStartDate = jobStartDate,
                account = user,
                manager = manager,
                jobEndDate = jobEndDate,
                maritalStatus = maritalStatus
            )
        }
        return employeeRepository.save(employee)
    }

    fun getByAccountId(accountId: String): Employee {
        return employeeRepository.findByAccountId(accountId) ?: throw ResourceNotFoundException(EMPLOYEE_NOT_FOUND)
    }

    fun getEmployeeProfile(accountId: String): EmployeeProfileResponseDto {
        val employee = getByAccountId(accountId)
        return conversionService.convert { employee }
    }

    fun getAllByDepartment(departmentId: String): List<Employee> {
        return employeeRepository.findAllByDepartmentId(departmentId)
    }

    fun attendanceRequest(accountId: String, employeeAttendanceRequest: AttendanceCreateRequestDto): Attendance {
        val employee = employeeRepository.findByAccountId(accountId) ?: throw ResourceNotFoundException()
        return attendanceService.create(employeeAttendanceRequest, employee)
    }

    fun update(employee: Employee): Employee {
        return employeeRepository.save(employee)
    }

    fun update(id: String, employeeRequest: EmployeeUpdateRequestDto): Employee {
        val employee = getById(id)
        val account = userService.update(employee.account.copy(email = employeeRequest.email, roles = employeeRequest.roles))
        val manager = employeeRepository.findByIdOrNull(employeeRequest.manager)
        val position = positionService.findByNameOrCreate(employeeRequest.position)
        val department = departmentService.findByNameOrCreate(employeeRequest.department)
        val updatedEmployee = with(employeeRequest) {
            employee.copy(
                name = name,
                surname = surname,
                email = email,
                birthDate = birthDate,
                title = title,
                position = position,
                department = department,
                phoneNumber = phoneNumber,
                address = address,
                jobStartDate = jobStartDate,
                account = account,
                manager = manager,
                jobEndDate = jobEndDate,
                maritalStatus = maritalStatus
            )
        }
        return employeeRepository.save(updatedEmployee)
    }

    companion object {
        private const val EMPLOYEE_NOT_FOUND = "error.employee.not-found"
    }
}