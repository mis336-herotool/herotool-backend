package com.herotool.herotool.service

import com.herotool.herotool.domain.dto.request.ChangePasswordRequestDto
import com.herotool.herotool.domain.enums.UserRole
import com.herotool.herotool.domain.model.User
import com.herotool.herotool.repository.UserRepository
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service

@Service
class UserService(private val userRepository: UserRepository, private val passwordEncoder: PasswordEncoder) : UserDetailsService {

    override fun loadUserByUsername(email: String): UserDetails {
        return userRepository.getByEmail(email)
    }

    fun getById(id: String) = userRepository.getById(id)

    fun create(email: String, roles: List<UserRole> = listOf(UserRole.EMPLOYEE)): User {
        val password = passwordEncoder.encode("password")
        val user = User(email, password, roles)
        return userRepository.save(user)
    }

    fun findByEmail(email: String) = userRepository.findByEmail(email)

    fun update(user: User): User {
        return userRepository.save(user)
    }

    fun changePassword(user: User?, request: ChangePasswordRequestDto) {
        requireNotNull(user)

        if(!passwordEncoder.matches(request.previousPassword, user.password)) throw BadCredentialsException("Wrong password")

        userRepository.save(user.copy(password = passwordEncoder.encode(request.newPassword)))
    }
}