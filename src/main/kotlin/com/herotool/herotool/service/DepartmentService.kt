package com.herotool.herotool.service

import com.herotool.herotool.domain.model.Department
import com.herotool.herotool.extension.findByIdOrNull
import com.herotool.herotool.repository.DepartmentRepository
import org.springframework.stereotype.Service

@Service
class DepartmentService(private val departmentRepository: DepartmentRepository) {

    fun create(department: Department): Department {
        return departmentRepository.save(department)
    }

    fun findById(id: String): Department? {
        return departmentRepository.findByIdOrNull(id)
    }

    fun findByName(departmentName: String): Department? {
        return departmentRepository.findByName(departmentName)
    }

    fun findByNameOrCreate(departmentName: String): Department {
        return findByName(departmentName) ?: create(Department(departmentName))
    }
}