package com.herotool.herotool.exception

abstract class BaseException(message: String, val args: Array<out Any>): RuntimeException(message)