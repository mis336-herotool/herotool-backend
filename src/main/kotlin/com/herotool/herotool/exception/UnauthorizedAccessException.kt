package com.herotool.herotool.exception

class UnauthorizedAccessException(message: String = "error.unauthorized", vararg args: Any): BaseException(message, args)