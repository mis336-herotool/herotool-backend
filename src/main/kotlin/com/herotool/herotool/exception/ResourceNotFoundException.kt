package com.herotool.herotool.exception

class ResourceNotFoundException(message: String = "error.resource.not-found", vararg args: Any) : BaseException(message, args)