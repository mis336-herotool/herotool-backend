package com.herotool.herotool.repository

import com.herotool.herotool.domain.model.Benefit
import com.herotool.herotool.domain.model.QBenefit
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.data.querydsl.QuerydslPredicateExecutor
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer
import org.springframework.data.querydsl.binding.QuerydslBindings

interface BenefitRepository : MongoRepository<Benefit, String>, QuerydslPredicateExecutor<Benefit>, QuerydslBinderCustomizer<QBenefit> {

    fun findByName(name: String): Benefit?

    @JvmDefault
    override fun customize(bindings: QuerydslBindings, root: QBenefit) {
    }
}