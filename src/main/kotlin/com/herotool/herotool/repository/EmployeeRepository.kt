package com.herotool.herotool.repository

import com.herotool.herotool.domain.model.Employee
import com.herotool.herotool.domain.model.QEmployee
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.data.querydsl.QuerydslPredicateExecutor
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer
import org.springframework.data.querydsl.binding.QuerydslBindings

interface EmployeeRepository : MongoRepository<Employee, String>, QuerydslPredicateExecutor<Employee>, QuerydslBinderCustomizer<QEmployee> {
    fun findByAccountId(accountId: String): Employee?

    fun findAllByManagerId(managerId: String): List<Employee>

    fun findAllByDepartmentId(departmentId: String): List<Employee>

    @JvmDefault
    override fun customize(bindings: QuerydslBindings, root: QEmployee) {

    }
}