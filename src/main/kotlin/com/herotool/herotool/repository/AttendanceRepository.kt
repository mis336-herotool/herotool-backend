package com.herotool.herotool.repository

import com.herotool.herotool.domain.enums.AttendanceStatus
import com.herotool.herotool.domain.model.Attendance
import com.herotool.herotool.domain.model.QAttendance
import com.querydsl.core.types.dsl.EnumExpression
import com.querydsl.core.types.dsl.StringExpression
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.data.querydsl.QuerydslPredicateExecutor
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer
import org.springframework.data.querydsl.binding.QuerydslBindings

interface AttendanceRepository : MongoRepository<Attendance, String>, QuerydslPredicateExecutor<Attendance>, QuerydslBinderCustomizer<QAttendance> {

    fun findAllByEmployeeIdIn(employeeIds: List<String>, pageable: Pageable): Page<Attendance>

    fun findAllByEmployeeId(employeeId: String, pageable: Pageable): Page<Attendance>

    fun findAllByEmployeeId(employeeId: String): List<Attendance>

    @JvmDefault
    override fun customize(bindings: QuerydslBindings, root: QAttendance) {
        bindings.run {
            with(root) {
                bind(employee.id).first(StringExpression::eq)
                bind(status).first(EnumExpression<AttendanceStatus>::eq)
                bind(reason).first(StringExpression::containsIgnoreCase)
            }
        }
    }
}