package com.herotool.herotool.repository

import com.herotool.herotool.domain.model.Position
import org.springframework.data.mongodb.repository.MongoRepository

interface PositionRepository: MongoRepository<Position, String> {

    fun findByName(positionName: String): Position?
}