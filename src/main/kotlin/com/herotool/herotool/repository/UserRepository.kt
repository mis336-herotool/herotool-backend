package com.herotool.herotool.repository

import com.herotool.herotool.domain.model.User
import org.springframework.data.mongodb.repository.MongoRepository

interface UserRepository: MongoRepository<User, String> {
    fun findByEmail(email: String): User?

    fun getByEmail(email: String): User

    fun getById(id: String): User
}