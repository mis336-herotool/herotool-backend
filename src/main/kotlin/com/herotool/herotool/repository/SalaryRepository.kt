package com.herotool.herotool.repository

import com.herotool.herotool.domain.model.Salary
import org.springframework.data.mongodb.repository.MongoRepository

interface SalaryRepository: MongoRepository<Salary, String>