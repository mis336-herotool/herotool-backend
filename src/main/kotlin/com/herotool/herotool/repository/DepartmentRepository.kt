package com.herotool.herotool.repository

import com.herotool.herotool.domain.model.Department
import org.springframework.data.mongodb.repository.MongoRepository

interface DepartmentRepository : MongoRepository<Department, String> {

    fun findByName(departmentName: String): Department?
}