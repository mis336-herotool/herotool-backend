package com.herotool.herotool.controller

import com.herotool.herotool.domain.dto.request.LoginRequestDto
import com.herotool.herotool.domain.dto.response.LoginResponseDto
import com.herotool.herotool.domain.model.User
import com.herotool.herotool.domain.enums.UserRole
import com.herotool.herotool.service.AuthService
import org.springframework.http.ResponseEntity
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

@RestController
@RequestMapping("auth")
class AuthController(private val authService: AuthService) {

    @PostMapping("/login")
    fun login(@Valid @RequestBody loginRequest: LoginRequestDto): ResponseEntity<LoginResponseDto> {
        val (email, password) = loginRequest
        val response = LoginResponseDto(authService.login(email, password))
        return ResponseEntity.ok(response)
    }
}