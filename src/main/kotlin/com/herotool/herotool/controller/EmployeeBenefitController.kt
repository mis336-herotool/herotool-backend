package com.herotool.herotool.controller

import com.herotool.herotool.domain.dto.request.EmployeeBenefitUpdateRequestDto
import com.herotool.herotool.domain.dto.response.EmployeeBenefitResponseDto
import com.herotool.herotool.domain.model.Employee
import com.herotool.herotool.extension.toPagedModel
import com.herotool.herotool.repository.EmployeeRepository
import com.herotool.herotool.service.EmployeeBenefitService
import com.querydsl.core.types.Predicate
import org.springframework.data.domain.Pageable
import org.springframework.data.querydsl.binding.QuerydslPredicate
import org.springframework.data.web.PageableDefault
import org.springframework.data.web.PagedResourcesAssembler
import org.springframework.hateoas.EntityModel
import org.springframework.hateoas.PagedModel
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("employee-benefits")
@PreAuthorize("hasAnyRole('ADMIN', 'HR')")
class EmployeeBenefitController(private val employeeBenefitService: EmployeeBenefitService) {

    @GetMapping("/{id}")
    fun getById(@PathVariable("id") employeeId: String): ResponseEntity<EmployeeBenefitResponseDto> {
        return ResponseEntity.ok(employeeBenefitService.getById(employeeId))
    }

    @GetMapping
    fun getAll(
        @PageableDefault pageable: Pageable,
        pagedResourcesAssembler: PagedResourcesAssembler<EmployeeBenefitResponseDto>,
        @QuerydslPredicate(root = Employee::class, bindings = EmployeeRepository::class) predicate: Predicate?
    ): ResponseEntity<PagedModel<EntityModel<EmployeeBenefitResponseDto>>> {
        return ResponseEntity.ok(pagedResourcesAssembler.toPagedModel(employeeBenefitService.getAll(predicate, pageable)))
    }

    @PutMapping("/{id}")
    fun update(@PathVariable("id") id: String, @RequestBody updateRequest: EmployeeBenefitUpdateRequestDto): ResponseEntity<EmployeeBenefitResponseDto> {
        return ResponseEntity.ok(employeeBenefitService.update(id, updateRequest))
    }
}