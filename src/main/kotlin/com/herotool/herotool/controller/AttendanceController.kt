package com.herotool.herotool.controller

import com.herotool.herotool.domain.dto.request.AttendanceCreateRequestDto
import com.herotool.herotool.domain.dto.request.AttendanceUpdateRequestDto
import com.herotool.herotool.domain.dto.response.AttendanceResponseDto
import com.herotool.herotool.domain.model.Attendance
import com.herotool.herotool.exception.ResourceNotFoundException
import com.herotool.herotool.extension.convert
import com.herotool.herotool.extension.toPagedModel
import com.herotool.herotool.repository.AttendanceRepository
import com.herotool.herotool.service.AttendanceService
import com.herotool.herotool.service.EmployeeService
import com.herotool.herotool.utility.getPrincipal
import com.querydsl.core.types.Predicate
import org.springframework.core.convert.ConversionService
import org.springframework.data.domain.Pageable
import org.springframework.data.querydsl.binding.QuerydslPredicate
import org.springframework.data.web.PageableDefault
import org.springframework.data.web.PagedResourcesAssembler
import org.springframework.hateoas.EntityModel
import org.springframework.hateoas.PagedModel
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("attendance")
class AttendanceController(
    private val conversionService: ConversionService,
    private val employeeService: EmployeeService,
    private val attendanceService: AttendanceService
) {

    @GetMapping("/{id}")
    @PreAuthorize("isAuthenticated()")
    fun getById(@PathVariable("id") id: String): ResponseEntity<AttendanceResponseDto> {
        return ResponseEntity.ok(conversionService.convert { attendanceService.getById(id) })
    }

    @GetMapping
    @PreAuthorize("isAuthenticated()")
    fun getAll(
        @PageableDefault pageable: Pageable,
        pagedResourcesAssembler: PagedResourcesAssembler<AttendanceResponseDto>,
        @QuerydslPredicate(root = Attendance::class, bindings = AttendanceRepository::class) predicate: Predicate?
    ): ResponseEntity<PagedModel<EntityModel<AttendanceResponseDto>>> {
        val page = attendanceService.getAll(pageable, predicate).map { conversionService.convert<AttendanceResponseDto> { it } }
        return ResponseEntity.ok(pagedResourcesAssembler.toPagedModel(page))
    }

    @PostMapping
    @PreAuthorize("hasAnyRole('EMPLOYEE')")
    fun create(@RequestBody employeeCreateAttendanceRequestDto: AttendanceCreateRequestDto): ResponseEntity<AttendanceResponseDto> {
        val user = getPrincipal() ?: throw ResourceNotFoundException()
        val attendance = employeeService.attendanceRequest(user.id!!, employeeCreateAttendanceRequestDto)
        return ResponseEntity.ok(conversionService.convert { attendance })
    }

    @PutMapping("/{id}")
    @PreAuthorize("isAuthenticated()")
    fun update(@PathVariable("id") id: String, @RequestBody updateRequest: AttendanceUpdateRequestDto): ResponseEntity<AttendanceResponseDto> {
        val response = attendanceService.update(id, updateRequest)
        return ResponseEntity.ok(conversionService.convert { response })
    }

    @GetMapping("/pending")
    @PreAuthorize("hasRole('MANAGEMENT')")
    fun getPendingRequests(): ResponseEntity<List<AttendanceResponseDto>> {
        val user = getPrincipal()
        val response = attendanceService.getPendingRequests(user?.id!!)
            .map { conversionService.convert<AttendanceResponseDto> { it } }
        return ResponseEntity.ok(response)
    }
}