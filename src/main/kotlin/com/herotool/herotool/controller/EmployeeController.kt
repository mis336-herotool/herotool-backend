package com.herotool.herotool.controller

import com.herotool.herotool.domain.dto.request.ChangePasswordRequestDto
import com.herotool.herotool.domain.dto.request.EmployeeCreateRequestDto
import com.herotool.herotool.domain.dto.request.EmployeeUpdateRequestDto
import com.herotool.herotool.domain.dto.response.EmployeeProfileResponseDto
import com.herotool.herotool.domain.dto.response.EmployeeResponseDto
import com.herotool.herotool.domain.model.Employee
import com.herotool.herotool.extension.convert
import com.herotool.herotool.extension.convertResponseBody
import com.herotool.herotool.extension.toPagedModel
import com.herotool.herotool.repository.EmployeeRepository
import com.herotool.herotool.service.EmployeeService
import com.herotool.herotool.service.UserService
import com.herotool.herotool.utility.getPrincipal
import com.querydsl.core.types.Predicate
import org.springframework.core.convert.ConversionService
import org.springframework.data.domain.Pageable
import org.springframework.data.querydsl.binding.QuerydslPredicate
import org.springframework.data.web.PageableDefault
import org.springframework.data.web.PagedResourcesAssembler
import org.springframework.hateoas.EntityModel
import org.springframework.hateoas.PagedModel
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("employees")
class EmployeeController(
    private val employeeService: EmployeeService,
    private val userService: UserService,
    private val conversionService: ConversionService
) {

    @GetMapping("/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'HR', 'MANAGEMENT')")
    fun getById(@PathVariable("id") id: String): ResponseEntity<EmployeeResponseDto> {
        return conversionService.convertResponseBody { employeeService.getById(id) }
    }

    @GetMapping("/all")
    @PreAuthorize("hasAnyRole('ADMIN', 'HR', 'MANAGEMENT')")
    fun getAllWithoutPagination(): ResponseEntity<List<EmployeeResponseDto>> {
        val responseBody = employeeService.findAll().map { conversionService.convert<EmployeeResponseDto> { it } }
        return ResponseEntity.ok(responseBody)
    }

    @GetMapping
    @PreAuthorize("hasAnyRole('ADMIN', 'HR', 'MANAGEMENT')")
    fun getAll(
        @PageableDefault pageable: Pageable,
        pagedResourcesAssembler: PagedResourcesAssembler<EmployeeResponseDto>,
        @QuerydslPredicate(root = Employee::class, bindings = EmployeeRepository::class) predicate: Predicate?
    ): ResponseEntity<PagedModel<EntityModel<EmployeeResponseDto>>> {
        val responseBody = employeeService.getAll(predicate, pageable).map { conversionService.convert<EmployeeResponseDto> { it } }
        return ResponseEntity.ok(pagedResourcesAssembler.toPagedModel(responseBody))
    }

    @PostMapping
    @PreAuthorize("hasAnyRole('ADMIN', 'HR')")
    fun create(@RequestBody employeeCreateRequestDto: EmployeeCreateRequestDto): ResponseEntity<EmployeeResponseDto> {
        return conversionService.convertResponseBody { employeeService.create(employeeCreateRequestDto) }
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'HR')")
    fun update(@PathVariable("id") id: String, @RequestBody employeeUpdateRequestDto: EmployeeUpdateRequestDto): ResponseEntity<EmployeeResponseDto> {
        return conversionService.convertResponseBody { employeeService.update(id, employeeUpdateRequestDto) }
    }

    @PutMapping("/change-password")
    @PreAuthorize("isAuthenticated()")
    fun changePassword(@RequestBody changePasswordRequestDto: ChangePasswordRequestDto): ResponseEntity<Nothing> {
        userService.changePassword(getPrincipal(), changePasswordRequestDto)
        return ResponseEntity.noContent().build()
    }

    @GetMapping("/me")
    @PreAuthorize("isAuthenticated()")
    fun getEmployeeProfile(): ResponseEntity<EmployeeProfileResponseDto> {
        val user = getPrincipal()
        return ResponseEntity.ok(employeeService.getEmployeeProfile(user?.id!!))
    }
}