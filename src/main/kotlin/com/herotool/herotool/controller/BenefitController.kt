package com.herotool.herotool.controller

import com.herotool.herotool.domain.dto.request.BenefitCreateRequestDto
import com.herotool.herotool.domain.dto.request.BenefitUpdateRequestDto
import com.herotool.herotool.domain.dto.response.BenefitProfileResponseDto
import com.herotool.herotool.domain.dto.response.BenefitResponseDto
import com.herotool.herotool.domain.model.Benefit
import com.herotool.herotool.extension.convert
import com.herotool.herotool.extension.convertResponseBody
import com.herotool.herotool.extension.toPagedModel
import com.herotool.herotool.repository.BenefitRepository
import com.herotool.herotool.service.BenefitService
import com.herotool.herotool.utility.getPrincipal
import com.querydsl.core.types.Predicate
import org.springframework.core.convert.ConversionService
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.querydsl.binding.QuerydslPredicate
import org.springframework.data.web.PageableDefault
import org.springframework.data.web.PagedResourcesAssembler
import org.springframework.data.web.SortDefault
import org.springframework.hateoas.EntityModel
import org.springframework.hateoas.PagedModel
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("benefits")
class BenefitController(private val benefitService: BenefitService, private val conversionService: ConversionService) {

    @GetMapping("/{id}")
    @PreAuthorize("hasAnyRole('HR', 'ADMIN')")
    fun getById(@PathVariable("id") id: String): ResponseEntity<BenefitResponseDto> {
        return conversionService.convertResponseBody { benefitService.getById(id) }
    }

    @GetMapping("/all")
    @PreAuthorize("hasAnyRole('HR', 'ADMIN')")
    fun getAllWithoutPagination(): ResponseEntity<List<BenefitResponseDto>> {
        return ResponseEntity.ok(
            benefitService.getAllWithoutPagination()
                .map { conversionService.convert<BenefitResponseDto> { it } }
        )
    }

    @GetMapping
    @PreAuthorize("hasAnyRole('HR', 'ADMIN')")
    fun getAll(
        @PageableDefault pageable: Pageable,
        pagedResourcesAssembler: PagedResourcesAssembler<BenefitResponseDto>,
        @QuerydslPredicate(root = Benefit::class, bindings = BenefitRepository::class) predicate: Predicate?
    ): ResponseEntity<PagedModel<EntityModel<BenefitResponseDto>>> {
        val page: Page<BenefitResponseDto> = benefitService.getAll(predicate, pageable).map { conversionService.convert<BenefitResponseDto> { it } }
        return ResponseEntity.ok(pagedResourcesAssembler.toPagedModel(page))
    }

    @PostMapping
    @PreAuthorize("hasAnyRole('HR', 'ADMIN')")
    fun create(@RequestBody benefitCreateRequest: BenefitCreateRequestDto): ResponseEntity<BenefitResponseDto> {
        return conversionService.convertResponseBody { benefitService.create(benefitCreateRequest) }
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAnyRole('HR', 'ADMIN')")
    fun update(@PathVariable("id") id: String, @RequestBody benefitUpdateRequest: BenefitUpdateRequestDto): ResponseEntity<BenefitResponseDto> {
        return conversionService.convertResponseBody { benefitService.update(id, benefitUpdateRequest) }
    }

    @GetMapping("/me")
    @PreAuthorize("hasRole('EMPLOYEE')")
    fun getProfile(): ResponseEntity<BenefitProfileResponseDto> {
        val account = getPrincipal() ?: throw IllegalStateException()
        return ResponseEntity.ok(benefitService.getBenefitProfile(account))
    }
}