package com.herotool.herotool.configuration

import com.herotool.herotool.domain.dto.response.ErrorResponseDto
import com.herotool.herotool.exception.BaseException
import com.herotool.herotool.exception.ResourceNotFoundException
import com.herotool.herotool.exception.UnauthorizedAccessException
import org.slf4j.LoggerFactory
import org.springframework.context.MessageSource
import org.springframework.context.i18n.LocaleContextHolder
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler

@ControllerAdvice
class GlobalExceptionHandler(private val messageSource: MessageSource) {

    private val logger by lazy { LoggerFactory.getLogger(this::class.java) }

    @ExceptionHandler(UnauthorizedAccessException::class)
    fun unauthorizedExceptionHandler(e: Exception) =
        exceptionHandler(HttpStatus.UNAUTHORIZED, e)

    @ExceptionHandler(ResourceNotFoundException::class)
    fun notFoundExceptionHandler(e: Exception) =
        exceptionHandler(HttpStatus.NOT_FOUND, e)

    private fun exceptionHandler(status: HttpStatus, e: Exception): ResponseEntity<ErrorResponseDto> {
        val errorMessage = this.getMessage(e)
        val errorResponseDto = ErrorResponseDto(status = status.toString(), text = status.name, message = errorMessage)

        return ResponseEntity.status(status).body(errorResponseDto)
    }

    private fun getMessage(e: Exception): String {
        if (e is BaseException) {
            return messageSource.getMessage(e.message.toString(), e.args.map { it.toString() }.toTypedArray(), LocaleContextHolder.getLocale())
        }

        return e.message.toString()
    }
}