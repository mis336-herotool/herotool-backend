package com.herotool.herotool.configuration

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.support.ConversionServiceFactoryBean
import org.springframework.core.convert.converter.Converter

@Configuration
class ConversionConfiguration {

    @Bean
    fun conversionService(converters: Set<Converter<*, *>>) = ConversionServiceFactoryBean().apply { setConverters(converters) }
}