package com.herotool.herotool.configuration.security.auth

object SecurityConstants {
    const val TOKEN_PREFIX = "Bearer "
    const val AUTHORIZATION_HEADER = "Authorization"
}