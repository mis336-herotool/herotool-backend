package com.herotool.herotool.configuration.security.auth

import com.herotool.herotool.configuration.properties.AuthenticationProperties
import com.herotool.herotool.utility.Logger
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.util.matcher.AntPathRequestMatcher
import org.springframework.web.filter.OncePerRequestFilter
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class JwtAuthenticationFilter(
    private val authenticationProperties: AuthenticationProperties,
    private val authenticationManager: AuthenticationManager
): OncePerRequestFilter() {

    private val log by Logger()

    override fun doFilterInternal(request: HttpServletRequest, response: HttpServletResponse, chain: FilterChain) {
        log.debug("Filtering request with path: ${request.servletPath}")
        val authorizationHeader = request.getHeader(SecurityConstants.AUTHORIZATION_HEADER)
        val token = authorizationHeader?.removePrefix(SecurityConstants.TOKEN_PREFIX)

        if (!token.isNullOrBlank()) {
            try {
                authenticate(token)
            } catch (e: Exception) {
                return
            }
        }

        chain.doFilter(request, response)
    }

    override fun shouldNotFilter(request: HttpServletRequest): Boolean {
        return request.antMatcher(*authenticationProperties.publicEndpoints.toTypedArray())
    }

    private fun authenticate(token: String) {
        val authentication = authenticationManager.authenticate(JwtAuthenticationToken(token))
        SecurityContextHolder.getContext().authentication = authentication
    }

    private fun HttpServletRequest.antMatcher(vararg prefix: String): Boolean {
        val antMatchers = prefix.map { AntPathRequestMatcher(it) }
        return antMatchers
            .any { it.matches(this) }
            .also { log.debug("Request path: ${this.servletPath}, should filter: $it") }
    }
}