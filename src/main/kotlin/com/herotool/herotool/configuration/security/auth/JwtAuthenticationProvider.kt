package com.herotool.herotool.configuration.security.auth

import com.herotool.herotool.configuration.properties.AuthenticationProperties
import com.herotool.herotool.service.TokenService
import com.herotool.herotool.service.UserService
import com.herotool.herotool.utility.Logger
import io.jsonwebtoken.Claims
import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.core.Authentication
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.stereotype.Component

@Component
class JwtAuthenticationProvider(
    private val tokenService: TokenService,
    private val userService: UserService,
    private val authenticationProperties: AuthenticationProperties
) : AuthenticationProvider {

    private val logger by Logger()

    override fun authenticate(authentication: Authentication): Authentication {
        return with(authentication as JwtAuthenticationToken) {
            val claims = tokenService.getClaims(jwtToken)
            val userId = claims.getUserId()
            if (userId == authenticationProperties.superAdminKey) {
                JwtAuthenticationToken(jwtToken, null, true)
            } else {
                val user = getUser(userId)
                user.generateAuthenticationToken(jwtToken)
            }
        }
    }

    override fun supports(authentication: Class<*>): Boolean {
        return authentication.isAssignableFrom(JwtAuthenticationToken::class.java)
    }

    private fun <T : UserDetails> T.generateAuthenticationToken(token: String): JwtAuthenticationToken {
        return JwtAuthenticationToken(token, this, true)
    }

    private fun Claims.getUserId(): String {
        return this["id"]!! as String
    }

    private fun getUser(userId: String) = userService.getById(userId)

    companion object {
        private const val INVALID_USER = "error.auth.invalid-user"
    }
}