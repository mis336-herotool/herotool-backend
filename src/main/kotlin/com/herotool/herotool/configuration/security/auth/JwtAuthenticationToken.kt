package com.herotool.herotool.configuration.security.auth

import com.herotool.herotool.domain.model.User
import org.springframework.security.core.Authentication
import org.springframework.security.core.GrantedAuthority

class JwtAuthenticationToken(
    val jwtToken: String,
    private val principal: Any? = null,
    private var authenticated: Boolean = false
) : Authentication {

    override fun getAuthorities(): List<GrantedAuthority> {
        if (principal == null) return listOf(GrantedAuthority { "ROLE_ADMIN" })
        val user = principal as User
        return user.authorities
    }

    override fun setAuthenticated(isAuthenticated: Boolean) {
        this.authenticated = isAuthenticated
    }

    override fun getName(): String? {
        return null
    }

    override fun getCredentials(): Any? {
        return null
    }

    override fun getPrincipal(): Any? {
        return principal
    }

    override fun isAuthenticated(): Boolean {
        return authenticated
    }

    override fun getDetails(): Any? {
        return principal
    }
}