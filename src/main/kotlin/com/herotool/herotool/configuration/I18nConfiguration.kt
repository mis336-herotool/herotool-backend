package com.herotool.herotool.configuration

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.support.ReloadableResourceBundleMessageSource
import org.springframework.web.servlet.i18n.AcceptHeaderLocaleResolver
import java.util.Locale

@Configuration
class I18nConfiguration {

    @Bean
    fun localeResolver(): AcceptHeaderLocaleResolver =
        AcceptHeaderLocaleResolver().apply {
            defaultLocale = Locale("tr")
            supportedLocales = listOf(Locale("en"), Locale("tr"))
        }

    @Bean
    fun messageSource(): ReloadableResourceBundleMessageSource =
        ReloadableResourceBundleMessageSource().apply {
            setBasename("classpath:messages")
            setDefaultEncoding("UTF-8")
            setDefaultLocale(Locale("en"))
            setUseCodeAsDefaultMessage(true)
        }
}