package com.herotool.herotool.configuration.properties

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component

@Component
@ConfigurationProperties(prefix = "authentication")
class AuthenticationProperties {

    lateinit var privateKey: String
    lateinit var publicKey: String
    lateinit var publicEndpoints: List<String>
    lateinit var superAdminKey: String
}