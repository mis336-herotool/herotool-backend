package com.herotool.herotool.configuration

import com.google.i18n.phonenumbers.PhoneNumberUtil
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class UtilityConfiguration {

    @Bean
    fun phoneNumberUtils(): PhoneNumberUtil {
        return PhoneNumberUtil.getInstance()
    }
}