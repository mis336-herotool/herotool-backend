package com.herotool.herotool.utility

import com.herotool.herotool.configuration.properties.AuthenticationProperties
import com.herotool.herotool.domain.enums.UserRole
import com.herotool.herotool.service.TokenService
import org.springframework.boot.context.event.ApplicationStartedEvent
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component

@Component
class SuperUserKeyGenerator(private val authenticationProperties: AuthenticationProperties, private val tokenService: TokenService) {

    private val logger by Logger()

    @EventListener(ApplicationStartedEvent::class)
    fun createSuperUserKey() {
        val secretKey = authenticationProperties.superAdminKey
        val token = tokenService.generateToken(secretKey, listOf(UserRole.ADMIN))
        logger.info("SuperUser token:\n$token")
    }
}