package com.herotool.herotool.utility

import com.herotool.herotool.domain.enums.UserRole
import com.herotool.herotool.domain.model.User
import org.springframework.security.core.context.SecurityContextHolder
import java.math.BigDecimal

fun getPrincipal(): User? {
    val principal = SecurityContextHolder.getContext()?.authentication?.principal
    return if (principal != null && principal is User) {
        principal
    } else {
        null
    }
}

fun checkAuthority(role: UserRole): Boolean {
    val authorities = SecurityContextHolder.getContext()?.authentication?.authorities?.toList()
    return authorities?.any { it.authority.endsWith(role.name) } ?: false
}

fun List<BigDecimal>.average(): BigDecimal {
    return if (this.isEmpty()) {
        return BigDecimal.ZERO
    } else {
        this.reduce(BigDecimal::add)
            .div(BigDecimal(this.size))
    }
}
