FROM gradle:6.3-jdk11 AS build-env
COPY --chown=gradle:gradle . /home/gradle/src

WORKDIR /home/gradle/src
RUN gradle build --no-daemon

FROM openjdk:11-jre-slim AS runtime
ARG BUILD=/home/gradle/src/build/libs
COPY --from=build-env ${BUILD}/*.jar app.jar
ENTRYPOINT ["java", "-jar", "/app.jar"]